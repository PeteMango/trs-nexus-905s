#include "main.h"
#include "Declarations.hpp"
#define G06 MOTOR_GEARSET_06
#define G18 MOTOR_GEARSET_18
#define G36 MOTOR_GEARSET_36
#define t true
#define f false

pros::Controller M (CONTROLLER_MASTER);
pros::Motor LB (13,G06,t), RB (16,G06,f), RF (15,G06,f), LF (17,G06,t), A (11,G36,f), T (20,G36,f), L (19,G18,f), R (14,G18,t);
pros::ADIPotentiometer PA (8), PT (7);
armPositions armPos = controlArm;
tiltPositions tiltPos = controlTilt;
intakePositions itkPos = stop;
