#include "main.h"
#include "Declarations.hpp"
#include "Functions.hpp"

void initialize() {
  PA.calibrate();
  PT.calibrate();
}
void disabled() {}
void competition_initialize() {}

void opcontrol() {
	pros::Task DrvTask (Drv, (void*)"PROS", TASK_PRIORITY_DEFAULT, TASK_STACK_DEPTH_DEFAULT, "");
	pros::Task ItkTask (Itk, (void*)"PROS", TASK_PRIORITY_DEFAULT, TASK_STACK_DEPTH_DEFAULT, "");
	pros::Task TltTask (Tlt, (void*)"PROS", TASK_PRIORITY_DEFAULT, TASK_STACK_DEPTH_DEFAULT, "");
	pros::Task LftTask (Lft, (void*)"PROS", TASK_PRIORITY_DEFAULT, TASK_STACK_DEPTH_DEFAULT, "");
  pros::Task LftToTask (LftTo, (void*)"PROS", TASK_PRIORITY_DEFAULT, TASK_STACK_DEPTH_DEFAULT, "");
  pros::Task ItkToTask (ItkTo, (void*)"PROS", TASK_PRIORITY_DEFAULT, TASK_STACK_DEPTH_DEFAULT, "");
  pros::Task TltToTask (TltTo, (void*)"PROS", TASK_PRIORITY_DEFAULT, TASK_STACK_DEPTH_DEFAULT, "");
	while (true) {}
}
