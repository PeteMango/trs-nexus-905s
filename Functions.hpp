#include "main.h"

#ifndef FUNCTIONS_HPP
#define FUNCTIONS_HPP

void Drv (void*);
void Itk (void*);
void Lft (void*);
void Tlt (void*);

void TltTo (void*);
void LftTo (void*);
void ItkTo (void*);

#endif
