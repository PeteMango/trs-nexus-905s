#include "main.h"
#include "Declarations.hpp"
#include "Functions.hpp"
#include "okapi/api.hpp"

using namespace okapi;
auto chassis = ChassisControllerBuilder()
    .withMotors({17,13}, {15,16})
    .withDimensions(AbstractMotor::gearset::blue, {{3.25_in, 12.5_in}, imev5BlueTPR})
    .build();

void delay(int time) {
  int target = pros::millis() + time;

  while(true)
  {
    if(pros::millis() >= target)
    {
      break;
    }
  }
}
void spinIntake () {
  L.move(127);
  R.move(127);
}
void spinOutake () {
  L.move(-100); R.move(-100);
  delay(200);
  L.move_velocity(0); R.move_velocity(0);

}
void stack () {
  T.move(75);
  delay(2700);
}
void BlueS () {
  spinIntake();
  chassis->setMaxVelocity(230);
  chassis->moveDistance(190_cm);
  chassis->turnAngle(43_deg);
  chassis->setMaxVelocity(400);
  chassis->moveDistance(-170_cm);
  chassis->setMaxVelocity(200);
  chassis->turnAngle(-43_deg);
  chassis->moveDistance(200_cm);
  chassis->turnAngle(-200_deg);
  chassis->setMaxVelocity(400);
  chassis->moveDistance(175_cm);
  L.move(-70); R.move(-70);
  delay(400);
  L.move(0); R.move(0);
  LF.move(50); RF.move(50);
  LB.move(50); RB.move(50);
  stack();
  LF.move(0); RF.move(0);
  LB.move(0); RB.move(0);
  L.move(-127); R.move(-127);
  LF.move(-50); RF.move(-50);
  LB.move(-50); RB.move(-50);
  delay(500);
  LF.move(0); RF.move(0);
  LB.move(0); RB.move(0);
}
void RedS () {
  spinIntake();
  chassis->setMaxVelocity(230);
  chassis->moveDistance(190_cm);
  chassis->turnAngle(-40_deg);
  chassis->setMaxVelocity(400);
  chassis->moveDistance(-170_cm);
  chassis->setMaxVelocity(200);
  chassis->turnAngle(40_deg);
  chassis->moveDistance(200_cm);
  chassis->turnAngle(200_deg);
  chassis->setMaxVelocity(400);
  chassis->moveDistance(175_cm);
  L.move(-70); R.move(-70);
  delay(400);
  L.move(0); R.move(0);
  LF.move(50); RF.move(50);
  LB.move(50); RB.move(50);
  stack();
  LF.move(0); RF.move(0);
  LB.move(0); RB.move(0);
  L.move(-127); R.move(-127);
  LF.move(-50); RF.move(-50);
  LB.move(-50); RB.move(-50);
  delay(500);
  LF.move(0); RF.move(0);
  LB.move(0); RB.move(0);
}

void tower () {
  A.move(90);
  delay(800);
  A.move(0);
  L.move(-70); R.move(-70);
  delay(150);
  L.move(0); R.move(0);
  A.move(90);
  delay(1700);
  chassis->moveDistance(50_cm);
  A.move(90);
  L.move(70); R.move(70);
  delay(300);
  L.move(-127); R.move(-127);
  delay(1000);
  L.move(0); R.move(0);
  A.move(0);
  delay(200);
  chassis->moveDistance(-40_cm);
  A.move(-127);
  delay(3000);
  A.move(0);
}

void stopMovement() {
  L.move_velocity(0); R.move_velocity(0);
  A.move(0); T.move(0);
}

void skillsRun () {
  spinIntake();
  chassis->setMaxVelocity(150);
  chassis->moveDistance(430_cm);
  delay(200);
  chassis->turnAngle(70_deg);
  stopMovement();
  tower();
  chassis->turnAngle(-135_deg);
  chassis->moveDistance(190_cm);
  L.move(-70); R.move(-70);
  delay(400);
  L.move(0); R.move(0);
  LF.move(50); RF.move(50);
  LB.move(50); RB.move(50);
  stack();
  LF.move(0); RF.move(0);
  LB.move(0); RB.move(0);

  chassis->moveDistance(-50_cm);
}

void autonomous() {
  skillsRun();
}
