#include "main.h"
#include "Declarations.hpp"
#include "Functions.hpp"
const double kP = 0.3, kD = 0.9;
using namespace std;

int sgn (double v) {
  return (v < 0) ? -1 : ((v > 0) ? 1 : 0);
}
void Drv (void*) {
  int LS, RS, LP, RP;
  while(true) {
    LS = M.get_analog(ANALOG_LEFT_Y) + M.get_analog(ANALOG_LEFT_X)*0.7;
    RS = M.get_analog(ANALOG_LEFT_Y) - M.get_analog(ANALOG_LEFT_X)*0.7;

    LP = int(pow(LS, 2) / 127) * sgn(LS);
    RP = int(pow(RS, 2) / 127) * sgn(RS);

    LF.move(LP); LB.move(LP);
    RF.move(RP); RB.move(RP);
  }
}

void Itk (void*) {
  while(true) {
    if(M.get_digital(DIGITAL_R1)) {
      L.move(127); R.move(127);
    }
    else if(M.get_digital(DIGITAL_R2)) {
      L.move(-127); R.move(-127);
    }
    else {
      L.move_velocity(0); R.move_velocity(0);
      L.set_brake_mode(MOTOR_BRAKE_HOLD); R.set_brake_mode(MOTOR_BRAKE_HOLD);
    }
  }
}

void Lft (void*) {
  double Dst, totalE, prevE, Pwr, E, P, D;
  while(true) {
    if(M.get_digital(DIGITAL_Y)) {
      Dst = 1100;
        do {
          if(M.get_digital(DIGITAL_A)) { break; }
          totalE += E;
          prevE = E;
          E = Dst - PA.get_value();

          P = E;
          D = E - prevE;

          Pwr = P + D;
          A.move_velocity(Pwr);

          pros::delay(20);
        } while(abs(E) >= 50);
      A.set_brake_mode(MOTOR_BRAKE_HOLD);
    }
    else if(M.get_digital(DIGITAL_X)) {
      Dst = 1400;
        do {
          if(M.get_digital(DIGITAL_A)) { break; }
          totalE += E;
          prevE = E;
          E = Dst - PA.get_value();

          P = E;
          D = E - prevE;

          Pwr = P + D;
          A.move_velocity(Pwr);

          pros::delay(20);
        } while(abs(E) >= 50);
      A.set_brake_mode(MOTOR_BRAKE_HOLD);
    }
    else if(M.get_digital(DIGITAL_B)) {
      A.move(-127);
      pros::delay(2000);
      A.move(0);
    }
    else if(M.get_digital(DIGITAL_L1)) {
      A.move(127);
    }
    else if(M.get_digital(DIGITAL_L2)) {
      A.move(-127);
    }
    else {
      A.move_velocity(0);
      A.set_brake_mode(MOTOR_BRAKE_HOLD);
    }
  }
}

void Tlt (void*) {
  double Dst, totalE, prevE, Pwr, E, P, D;
  while(true) {
    if(M.get_digital(DIGITAL_UP)) {
      Dst = 400;
        do {
          totalE += E;
          prevE = E;
          E = Dst - PA.get_value();

          P = E * kP;
          D = (E - prevE) * kD;

          Pwr = P + D;
          T.move_velocity(Pwr);

          pros::delay(20);
        } while(abs(E) >= 50);
      T.move(60);
      pros::delay(300);
      T.move_velocity(0);
    }
    else if(M.get_digital(DIGITAL_DOWN)) {
      Dst = 100;
        do {
          totalE += E;
          prevE = E;
          E = Dst - PA.get_value();

          P = E * kP;
          D = (E - prevE) * kD;

          Pwr = P + D;
          T.move_velocity(Pwr);

          pros::delay(20);
        } while(abs(E) >= 50);
      T.move(-80);
      pros::delay(300);
      T.move_velocity(0);
    }
    else if(abs(M.get_analog(ANALOG_RIGHT_Y)) >= 10) {
      T.move(M.get_analog(ANALOG_RIGHT_Y) * 0.7);
    }
    else {
      T.move_velocity(0);
      T.set_brake_mode(MOTOR_BRAKE_HOLD);
    }
  }
}
