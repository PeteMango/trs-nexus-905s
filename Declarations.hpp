#include "main.h"

#ifndef DECLARATIONS_HPP
#define DECLARATIONS_HPP

extern pros::Controller M;
extern pros::Motor LF, RF, LB, RB, L, R, A, T;
extern pros::ADIPotentiometer PA, PT;

enum armPositions {controlArm, intake, lowTower, midTower};
extern armPositions armPos;
enum tiltPositions {controlTilt, startPos, stackPos};
extern tiltPositions tiltPos;
enum intakePositions {slowOtk, fastOtk, fastItk, stop};
extern intakePositions itkPos;

#endif
